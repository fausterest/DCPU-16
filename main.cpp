#include <iostream>
#include <fstream>
#include <vector>

#include "DCPU16/emulator.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    emulator e(argv[1]);

    e.run();

    return 0;
}