#include "instruction.hpp"

instruction::instruction(uint16_t val) : _val(val) {
    ((val & 0x1f) == 0) ? (_is_ext = true) : (_is_ext = false);

    _a  = static_cast<uint8_t>((val >> 10) & 0x3f);
    if (_is_ext) {
        _op = static_cast<uint8_t>((val >> 5) & 0x1f);
        _b = 0x0000;
    }
    else {
        _op = static_cast<uint8_t>(val & 0x1f);
        _b = static_cast<uint8_t>((val >> 5) & 0x1f);
    }
}

std::string instruction::information() const {
    std::stringstream out;
    out << "--- Instruction information ---" << std::endl;
    out << "Special flag: " << std::boolalpha << _is_ext << std::noboolalpha << std::endl;
    out << "In hex: " << std::hex << _val << "\n" << std::endl;
    std::bitset<6> inst_a(_a);
    std::bitset<5> inst_b(_b);
    std::bitset<5> inst_op(_op);

    if (_is_ext) {
        out << "Binary: "
            << inst_op.to_string() << "(op) "
            << inst_a.to_string() << "(a)" << std::endl;
        out << "Hex: " << std::hex
            << (int)_op << "(op) "
            << (int)_a << "(a)" << std::endl;
        out << "Op name: " << ext_opcodes_names[_op];
    }
    else {
        out << "Binary: "
            << inst_op.to_string() << "(op) "
            << inst_b.to_string() << "(b), "
            << inst_a.to_string() << "(a)" << std::endl;
        out << "Hex: " << std::hex
            << (int)_op << "(op) "
            << (int)_b << "(b), "
            << (int)_a << "(a)" << std::endl;
        out << "Op name: " << base_opcodes_names[_op];
    }

    return out.str();
}
