#ifndef DCPU_16_MEMORY_HPP
#define DCPU_16_MEMORY_HPP


#include <cstdint>
#include <array>
#include <sstream>


enum reg {
    r_A, r_B, r_C, r_X, r_Y, r_Z, r_I, r_J
};

const char reg_names[] = {
        'A', 'B', 'C', 'X', 'Y', 'Z', 'I', 'J'
};


template <class mem = std::array<uint16_t, 0x10000>,
          class reg = std::array<uint16_t, 8>>
class memory {
public:
    memory() : _memory_mapped(0) {}

    mem _memory;
    reg _registers;

    void memory_map(uint16_t memory_used) {
        _memory_mapped += memory_used;
    }

    std::string statistics() {
        std::stringstream out;
        out << "Memory mapped: "
            << _memory_mapped
            << " (" << _memory_mapped * 2 << " BYTES)\n";
        out << "Zero values: " << zero_values();
        return out.str();
    }

private:
    uint16_t zero_values() {
        uint16_t count;
        for (auto it : _memory) {
            if (it == 0)
                count++;
        }
        return count;
    }

    uint16_t _memory_mapped;
};


#endif //DCPU_16_MEMORY_HPP
