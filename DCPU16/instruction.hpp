#ifndef DCPU_16_INSTRUCTION_HPP
#define DCPU_16_INSTRUCTION_HPP


#include <cstdint>
#include <string>
#include <sstream>
#include <iostream>


const std::string base_opcodes_names[] = {
        "n/a", "SET", "ADD", "SUB",
        "MUL", "MLI", "DIV", "DVI",
        "MOD", "MDI", "AND", "BOR",
        "XOR", "SHR", "ASR", "SHL",
        "IFB", "IFC", "IFE", "IFN",
        "IFG", "IFA", "IFL", "IFU",
        "ERR", "ERR", "ADX", "SBX",
        "ERR", "ERR", "STI", "STD"
};

const std::string ext_opcodes_names[] = {
        "n/a", "JSR", "ERR", "ERR",
        "ERR", "ERR", "ERR", "ERR",
        "INT", "IAG", "IAS", "RFI",
        "IAQ", "ERR", "ERR", "ERR",
        "HWN", "HWQ", "HWI"
};


class instruction {
public:
    explicit instruction(uint16_t val);

    std::string information() const;

private:
    bool _is_ext;

    uint16_t _val;
    uint8_t _op;
    uint8_t _a;
    uint8_t _b;
};


#endif //DCPU_16_INSTRUCTION_HPP
