#ifndef DCPU_16_DCPU16_HPP
#define DCPU_16_DCPU16_HPP


#include <cstdint>
#include <memory>
#include <iostream>
#include <fstream>
#include <vector>
#include <array>

#include "hardware.hpp"
#include "memory.hpp"
#include "instruction.hpp"

static uint16_t lit[0x20] = {
        0xffff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
        0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
        0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
        0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e
};

enum base_o {
    O_EXT, O_SET, O_ADD, O_SUB,
    O_MUL, O_MLI, O_DIV, O_DVI,
    O_MOD, O_MDI, O_AND, O_BOR,
    O_XOR, O_SHR, O_ASR, O_SHL,
    O_IFB, O_IFC, O_IFE, O_IFN,
    O_IFG, O_IFA, O_IFL, O_IFU,
    O_ADX = 0x1a, O_SBX,
    O_STI = 0x1e, O_STD
};

enum ext_o {
    EO_EXT, O_JSR, O_INT = 0x08,
    O_IAG, O_IAS, O_RFI, O_IAQ,
    O_HWN = 0x10, O_HWQ, O_HWI
};


class DCPU16 : public hardware {
public:
    DCPU16(std::vector<uint16_t> &program)
            : hardware(0x0, 0x0, 0x0, program.size()), _pc(0), _sp(0), _ex(0), _o(0), _ia(0), _exit(true),
              _debug_console(true), _steps(0), _program_size(program.size())
    {
        _mem._memory.fill(0);
        std::copy(program.begin(), program.end(), _mem._memory.begin());
        _mem.memory_map(_program_size);
    }

    DCPU16() { };

//    TODO: JUST WTF
    void add_device(hardware* h) {
        hardware_connected.push_back(h);
    }

    bool run() {
        bool result = true;

        while (_exit && result) {
            if (_debug_console)
                _debug();

            result = _step();

            for (auto& it : hardware_connected) {
                it->step();
            }

            if (_pc >= _program_size)
                break;
        }
        if (_debug_console)
            _debug_make_memory_dump();
        return result;
    }

    bool step() {
        if (_debug_console)
            _debug();

        return _step();
    }

    ~DCPU16() {
    }

    memory<>& getMemory() {
        return _mem;
    }

private:
    bool _debug_console{};
    void _debug();
//    void _debug_basic_op(uint16_t o, uint16_t a, uint16_t b);
//    void _debug_ext_op(uint16_t o, uint16_t a);
    void _debug_make_memory_dump();

    bool _step();
    uint16_t* _operator(uint16_t val, bool bflag);
    void _basic_op(uint16_t op);
    void _ext_op(uint16_t op);
    void _skip_op(bool o_if);

    int32_t _tosigned(uint16_t val) {
        return val & 0x8000 ? val - 0x10000 : val;
    }

    bool _complex_op(uint8_t _op) {
        return (_op >= 0x10 && _op <= 0x17) ||
               (_op == 0x1a || _op == 0x1e || _op == 0x1f);
    }

    bool _exit{};

    std::vector<hardware *> hardware_connected;

    memory<> _mem;
    uint16_t _pc{};
    uint16_t _sp{};
    uint16_t _ex{};
    uint16_t _o{};
    uint16_t _ia{};

    uint32_t _steps{};
    uint32_t _program_size{};
};


#endif //DCPU_16_DCPU16_HPP
