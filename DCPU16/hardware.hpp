#ifndef DCPU_16_HARDWARE_HPP
#define DCPU_16_HARDWARE_HPP


#include <cstdint>

class hardware {
public:
    hardware() {}
    hardware(uint32_t id,
             uint16_t version,
             uint32_t manufacturer,
             uint16_t memory_used) :
            _id(id),
            _version(version),
            _manufacturer(manufacturer),
            _memory_used(memory_used) {}
    virtual ~hardware() {};

    virtual bool interruption() { return false; };

    virtual bool step() { return false; };

    uint32_t id() { return _id; }
    uint16_t version() { return _version; }
    uint32_t manufacturer() { return _manufacturer; };

protected:
    uint32_t _id{};
    uint16_t _version{};
    uint32_t _manufacturer{};
    uint16_t _memory_used{};
};


#endif //DCPU_16_HARDWARE_HPP
