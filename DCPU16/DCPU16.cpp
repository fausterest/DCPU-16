#include "DCPU16.hpp"

//        BASIC OPERATION: (op & 0x1f) != 0
//        Basic    = aaaa aabb bbbo oooo
//        Extended = aaaa aaoo ooo0 0000

void DCPU16::_debug() {
    std::cout << "-- Current step --" << std::endl;
    std::cout << _mem.statistics() << std::endl;
    std::cout << "Current step: " << _steps << " pc: " << _pc << std::endl;
    std::cout << "Reg print: " << std::endl;
    for (int i = 0; i < _mem._registers.size(); i++) {
        std::cout << std::hex << reg_names[i] << ": " << _mem._registers[i] << std::endl;
    }

    std::cout << "Mem print: " << std::endl;
    for (int i = std::max(_pc - 8, 0); i < _mem._memory.size() && i - _pc < 8; i++) {
        if (i == _pc)
            std::cout << std::hex << "[" << _mem._memory[i] << "] ";
        else
            std::cout << std::hex << _mem._memory[i] << " ";
    }
    std::cout << "\n";

    std::string in;
    while (std::cin >> in) {
        if (in == "c") {
            break;
        }
        if (in == "dump") {
            _debug_make_memory_dump();
        }
        if (in == "inst") {
            instruction inst(_mem._memory[_pc]);
            std::cout << inst.information() << std::endl;
        }
    }
}

bool DCPU16::_step() {
    _steps++;
    uint16_t val = _mem._memory[_pc++];

    if ((val & 0x1f) != 0) {
        _basic_op(val);
    }
    else {
        _ext_op(val);
    }

    return _pc != _program_size;
}

void DCPU16::_basic_op(uint16_t op) {
    uint16_t* a_addr = _operator(static_cast<uint16_t>((op >> 10) & 0x3f), false);
    uint16_t* b_addr = _operator(static_cast<uint16_t>((op >> 5) & 0x1f), true);
    uint16_t o = static_cast<uint16_t>(op & 0x1f);
    uint16_t a = *a_addr;
    uint16_t b = *b_addr;
    int32_t  r = 0x00;

//    if (_debug_console)
//        _debug_basic_op(
//                o,
//                static_cast<uint16_t>((op >> 10) & 0x3f),
//                static_cast<uint16_t>((op >> 5) & 0x1f)
//        );

    switch (o) {
        case O_SET:
            r = a;
            break;
        case O_ADD:
            r = a + b;
            _ex = static_cast<uint16_t>((r > 0xffff) ? 1 : 0);
            break;
        case O_SUB:
            r = b - a;
            _ex = static_cast<uint16_t>((r < 0) ? 0xffff : 0);
            break;
        case O_MUL:
            r = b * a;
            _ex = static_cast<uint16_t>(r >> 16);
            break;
        case O_MLI:
            r = _tosigned(a) * _tosigned(b);
            _ex = static_cast<uint16_t>(r >> 16);
            break;
        case O_DIV:
            if (a != 0) {
                r = b / a;
                _ex = static_cast<uint16_t>(((b << 16) / a) & 0xffff);
            }
            else {
                _ex = 0;
            }
            break;
        case O_DVI:
            if (a != 0) {
                r = _tosigned(b) / _tosigned(a);
                _ex = static_cast<uint16_t>(((_tosigned(b) << 16) / _tosigned(a)) & 0xffff);
            }
            else {
                _ex = 0;
            }
            break;
        case O_MOD:
            if (a != 0) {
                r = b % a;
            }
            break;
        case O_MDI:
            if(a != 0)
                r = _tosigned(b) % _tosigned(a);
            break;
        case O_AND:
            r = b & a;
            break;
        case O_BOR:
            r = b | a;
            break;
        case O_XOR:
            r = b ^ a;
            break;
        case O_SHR:
            r = b >> a;
            _ex = (b << 16) >> a;
            break;
        case O_ASR:
            r = _tosigned(b) << a;
            _ex = static_cast<uint16_t>((_tosigned(b) << 16) >> a);
            break;
        case O_SHL:
            r = b << a;
            _ex = static_cast<uint16_t>(r >> a);
            break;
        case O_IFB:
            if ((b & a) == 0) {
                _skip_op(true);
            }
            return;
        case O_IFC:
            if ((b & a) != 0) {
                _skip_op(true);
            }
            return;
        case O_IFE:
            if (b != a) {
                _skip_op(true);
            }
            return;
        case O_IFN:
            if (b == a) {
                _skip_op(true);
            }
            return;
        case O_IFG:
            if (b <= a) {
                _skip_op(true);
            }
            return;
        case O_IFA:
            if (_tosigned(b) <= _tosigned(a)) {
                _skip_op(true);
            }
            return;
        case O_IFL:
            if (b >= a) {
                _skip_op(true);
            }
            return;
        case O_IFU:
            if (_tosigned(b) >= _tosigned(a)) {
                _skip_op(true);
            }
            return;
        case O_ADX:
            r = b + a + _ex;
            _ex = static_cast<uint16_t>(r > 0xffff ? 1 : 0);
            break;
        case O_SBX:
            r = b - a + _ex;
            _ex = static_cast<uint16_t>(r < 0 ? 0xffff : 0);
            break;
        case O_STI:
            r = a;
            _mem._registers[r_I]++;
            _mem._registers[r_J]++;
            break;
        case O_STD:
            r = a;
            _mem._registers[r_I]--;
            _mem._registers[r_J]--;
            break;
        default:
            std::cerr << "Unknow opcode: " << o << std::endl;
            return;

    }

    if (((op >> 5) & 0x1f) < 0x1f) {
        *b_addr = static_cast<uint16_t>(r);
    }
}

void DCPU16::_ext_op(uint16_t op) {
    uint16_t * a_addr = _operator(static_cast<uint16_t>((op >> 10) & 0x3f), false);
    uint16_t o = static_cast<uint16_t>((op >> 5) & 0x1f);
    uint16_t a = *a_addr;
    std::cout << std::dec << O_HWN << std::endl;

//    if (_debug_console)
//        _debug_ext_op(o, static_cast<uint16_t>((op >> 10) & 0x3f));

    switch (o) {
        case O_JSR:
            _mem._memory[_sp] = _pc;
            _pc = a;
            return;
        case O_INT:
//            _interruption(_a);
            return;
        case O_IAG:
            *a_addr = _ia;
            return;
        case O_IAS:
            _ia = *a_addr;
            return;
        case O_RFI:
//            _int_queue_enabled = false;
            *a_addr = _mem._memory[_sp++];
            _pc = _mem._memory[_sp++];
            return;
        case O_IAQ:
//            _int_queue_enabled = (_a != 0);
            return;
        case O_HWN:
            *a_addr = static_cast<uint16_t>(hardware_connected.size());
            return;
        case O_HWQ:
            if (a < hardware_connected.size()) {
                _mem._registers[r_A] = static_cast<unsigned short>(hardware_connected[a]->id());
                _mem._registers[r_B] = static_cast<unsigned short>(hardware_connected[a]->id() >> 16);
                _mem._registers[r_C] = hardware_connected[a]->version();
                _mem._registers[r_X] = static_cast<unsigned short>(hardware_connected[a]->manufacturer());
                _mem._registers[r_Y] = static_cast<unsigned short>(hardware_connected[a]->manufacturer() >> 16);
            }
            else if (_debug_console) {
                std::cerr << "Unknown device! -> [" << a << "]. ";
                std::cerr << "There is only " << hardware_connected.size() << " connected device(s)";
            }
            return;
        case O_HWI:
            hardware_connected[a]->interruption();
            return;
        default:
            std::cerr << "Unknow opcode: " << o << std::endl;
            return;
    }
}

uint16_t* DCPU16::_operator(uint16_t val, bool bflag) {
    if (0x00 <= val && val <= 0x07) {
        return _mem._registers.data() + val;
    }
    else if (0x08 <= val && val <= 0x0f) {
        return _mem._memory.data() + _mem._registers[val & 7];
    }
    else if (0x10 <= val && val <= 0x17) {
        return  _mem._memory.data() + ((_mem._registers[val & 7] + _mem._memory[_pc++]) & 0xffff);
    }
    else if (val == 0x18) {
        if (bflag == true) {
            return _mem._memory.data() + (--_sp);
        }
        else {
            return _mem._memory.data() + _sp++;
        }
    }
    else if (val == 0x19) {
        return _mem._memory.data() + _sp;
    }
    else if (val == 0x1a) {
        return _mem._memory.data() + ((_sp + _mem._memory[_pc++]) & 0xffff);
    }
    else if (val == 0x1b) {
        return &_sp;
    }
    else if (val == 0x1c) {
        return &_pc;
    }
    else if (val == 0x1d) {
        return &_ex;
    }
    else if (val == 0x1e) {
        std::cout << (_mem._memory.data() + _mem._memory[_pc++]) << " " << *(_mem._memory.data() + _mem._memory[_pc++]) << std::endl;
        return _mem._memory.data() + _mem._memory[_pc++];
    }
    else if (val == 0x1f) {
        return _mem._memory.data() + _pc++;
    }
    else if (0x20 <= val && val <= 0x3f) {
        return (lit + (val & 0x1f));
    }
    else {
        return 0;
    }
}

void DCPU16::_skip_op(bool o_if) {
    uint16_t _val = _mem._memory[_pc++];

    if ((_val & 0x1f) != 0) {
        if (_complex_op(static_cast<uint8_t>((_val >> 10) & 0x3f)))
            _pc++;
        if (_complex_op(static_cast<uint8_t>((_val >> 5) & 0x1f)))
            _pc++;
    }
    else
    if (_complex_op(static_cast<uint8_t>((_val >> 10) & 0x3f)))
        _pc++;

    uint8_t _op = static_cast<uint8_t>(_val & 0x1f);
    if (o_if && (_op >= O_IFB && _op <= O_IFU))
        _skip_op(false);
}

void DCPU16::_debug_make_memory_dump() {
    std::ofstream ostream("memory_dump.txt");
    ostream << "0000 ";
    for (auto i = 1; i < _mem._memory.size() + 1; i++) {
        ostream.flags(std::ios::hex);
        ostream.fill('0');
        ostream.width(4);
        ostream << _mem._memory[i - 1] << " ";
        if (i % 8 == 0 && i != 0) {
            ostream << "\n";
            ostream.flags(std::ios::hex);
            ostream.fill('0');
            ostream.width(4);
            ostream << i << " ";
        }
    }

    std::cout << "Saved 'memory_dump.txt'" << std::endl;
}
