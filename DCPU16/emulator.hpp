#ifndef DCPU_16_EMULATOR_HPP
#define DCPU_16_EMULATOR_HPP


#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <list>

#include "DCPU16.hpp"
#include "hardware.hpp"

class emulator {
public:
    explicit emulator(char* binary_name) {
        std::ifstream a(binary_name);

        if (!a.is_open()) {
            std::cerr << "File test doesn't exit" << std::endl;
        }

        std::vector<uint16_t> p;
        uint16_t in;

        while (a >> std::hex >> in) {
            p.push_back(in);
        }

        _dcpu16 = DCPU16(p);
        _hardware.push_back(&_dcpu16);

        std::cout << "Type 'c' to continue execution,\n'dump' to make a memory dump,\n'inst' to look at instruction information" << std::endl;
    }

    void run() {
        bool running = true;
        while (running) {
            for (auto it : _hardware) {
                running = it->step();
            }
        }
    }

    ~emulator() {
        for (auto& it : _hardware) {
            delete it;
        }
    }

private:
    std::string _binary_name;

    DCPU16 _dcpu16;
    std::list<hardware *> _hardware;
};


#endif //DCPU_16_EMULATOR_HPP
